/*
 * Need to add port selection to GUI. - 2/6/11
 */

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class GraphPanel extends JPanel {
	private ArrayList <PhysicsPanel> leftJoy, rightJoy;
	private JPanel wholePanel, buttonPanel, leftJoyPanel, rightJoyPanel, joyPanel, labelPanel;
	private JButton line, point;
	private Timer timer;
	private static final int DELAY = 0;
	DatagramSocket dsocket;
	DatagramPacket packet;
	byte[] buffer;
	int port;
	
	public GraphPanel(int width, int height) {
		leftJoy = new ArrayList<PhysicsPanel>();
		rightJoy = new ArrayList<PhysicsPanel>();
		
		char[] type = {'z', 'y', 'x'};
		
		for(int i = 0; i < 3; i++) {
			PhysicsPanel l = new PhysicsPanel(0, 0, type[i], Color.BLACK);
			PhysicsPanel r = new PhysicsPanel(0, 0, type[i], Color.BLUE);
			leftJoy.add(l);
			rightJoy.add(r);
		}
		
		leftJoyPanel = new JPanel(new GridLayout(1, 3));
		leftJoyPanel.setPreferredSize(new Dimension(600, 300));
		rightJoyPanel = new JPanel(new GridLayout(1, 3));
		rightJoyPanel.setPreferredSize(new Dimension(600, 300));

		for(int i = 0; i < 3; i++) {
			leftJoyPanel.add(leftJoy.get(i));
			rightJoyPanel.add(rightJoy.get(i));
		}
		
		joyPanel = new JPanel(new GridLayout(2, 1));
		joyPanel.add(leftJoyPanel);
		joyPanel.add(rightJoyPanel);
		
		point = new JButton("Show Point");
		point.addActionListener(new ButtonListener());
		line = new JButton("Draw Cruve");
		line.addActionListener(new ButtonListener());    

		buttonPanel = new JPanel(new GridLayout(1, 2));
		buttonPanel.add(point);
		buttonPanel.add(line);
		buttonPanel.setPreferredSize(new Dimension(50,50));
		
		labelPanel = new JPanel(new GridLayout(1, 3));
		labelPanel.add(new JLabel("XY Plane"));
		labelPanel.add(new JLabel("XZ Plane"));
		labelPanel.add(new JLabel("YZ Plane"));
		
		wholePanel = new JPanel(new BorderLayout());
		wholePanel.add(buttonPanel, BorderLayout.PAGE_START);
		wholePanel.add(joyPanel, BorderLayout.CENTER);
		wholePanel.add(labelPanel, BorderLayout.PAGE_END);
		
		//int port = 21567;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Incoming Port:");
		port = scanner.nextInt();
		
		add(wholePanel);
		setPreferredSize(new Dimension(width,height));
		
		try {			
			dsocket = new DatagramSocket(port);
			buffer = new byte[1024];
			packet = new DatagramPacket(buffer, buffer.length);
			
		} catch(Exception e) {
			System.err.println(e);
		}	// end of try-catch
		
		timer = new Timer(DELAY, new MovingBallListener());
		timer.start();
	}	// end of ControlPanel
	
	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			Object action = event.getSource();
			
			if(action == point)
				for(int i = 0; i < 3; i++) {
					leftJoy.get(i).displayCurve(false);
					rightJoy.get(i).displayCurve(false);
				}
			else
				for(int i = 0; i < 3; i++) {
					leftJoy.get(i).displayCurve(true);
					rightJoy.get(i).displayCurve(true);
				}
		}
	}	// end of ButtonListener
	
	private class MovingBallListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				double[] pendaCoor = new double[6];
				dsocket.receive(packet);
				String msg = new String(buffer, 0, packet.getLength());
				//System.out.println(msg);
				String[] tokens = msg.split(",");
				
				for(int i = 0; i < 6; i++)
					pendaCoor[i] = Double.parseDouble(tokens[i]);

				packet.setLength(buffer.length);
				
				leftJoy.get(0).setAxis(pendaCoor[0], pendaCoor[1], pendaCoor[2]);
				leftJoy.get(1).setAxis(pendaCoor[0], pendaCoor[1], pendaCoor[2]);
				leftJoy.get(2).setAxis(pendaCoor[0], pendaCoor[1], pendaCoor[2]);
				
				rightJoy.get(0).setAxis(pendaCoor[3], pendaCoor[4], pendaCoor[5]);
				rightJoy.get(1).setAxis(pendaCoor[3], pendaCoor[4], pendaCoor[5]);
				rightJoy.get(2).setAxis(pendaCoor[3], pendaCoor[4], pendaCoor[5]);
			} catch(Exception ex) {
				System.err.println(ex);
			}
		}
	}	// end of MovingBallListener
}	// end of class ControlPanel
