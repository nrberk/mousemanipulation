/*
 * Need to add background image to display coordinates system. - 2/1/11
 */

import java.awt.*;
import java.lang.Math;
import javax.swing.*;

@SuppressWarnings("serial")
public class PhysicsPanel extends JPanel {
	private static final int CIRCLE_DIAMETER = 4;
	private int x, y;
	private char type;
	private boolean curve;
	
	public PhysicsPanel(int coorOne, int coorTwo, char t, Color c) {
		x = coorOne;
		y = coorTwo;
		type = t;
		
		//pointArray =  new ArrayList<Point>();
		curve = false;
		
		setBackground(c);
	}
	
	public void paintComponent(Graphics page) {
		page.setColor(Color.RED);

		if (!curve) {
			super.paintComponent(page);
			page.fillOval(x, y, CIRCLE_DIAMETER, CIRCLE_DIAMETER);
		}
		else{
			page.fillOval(x, y, CIRCLE_DIAMETER, CIRCLE_DIAMETER);
		}
	}
	 
	public void setAxis(double axisOne, double axisTwo, double axisThree) {
		double valueZ = 1 - axisThree;
		double angleOne, angleTwo;
		
		angleOne = axisOne * Math.PI / 6;
		angleTwo = axisTwo * Math.PI / 6;
		
		if(type == 'z') {
			x = (int)(valueZ * 100 * Math.cos(angleTwo) * Math.sin(angleOne)) + 100;
			y = (int)(valueZ * 100 * Math.cos(angleOne) * Math.sin(angleTwo)) + 100;
		}
		else if(type == 'y') {
			x = (int)(valueZ * 100 * Math.sin(angleOne) * Math.cos(angleTwo)) + 100;
			y = (int)(valueZ * 100 * Math.abs(Math.cos(angleOne)) * Math.abs(Math.cos(angleTwo)));
		}
		else if(type == 'x') {
			x = (int)(valueZ * 100 * Math.sin(angleTwo) * Math.cos(angleOne)) + 100;
			y = (int)(valueZ * 100 * Math.abs(Math.cos(angleOne)) * Math.abs(Math.cos(angleTwo)));
		}
		
		if(type == 'y') {
			System.out.println(axisOne + "  "+ axisTwo + "  "+ axisThree + " " + angleOne + " " + angleTwo + " " + valueZ);
			System.out.println(x + "-" + y);
		}
			
		updateUI();
	 }
	
	public void displayCurve(boolean d) {
		curve = d;
	}
}