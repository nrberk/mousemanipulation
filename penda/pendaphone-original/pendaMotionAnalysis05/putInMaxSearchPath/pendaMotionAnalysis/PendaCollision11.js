inlets = 4;
outlets = 7;

var ObjectCount = 2;
var MySphereCount = 2;

var mywindow = new JitterObject("jit.window","BounceScape2");
mywindow.depthbuffer = 1;
mywindow.size = [500,500];

var myrender = new JitterObject("jit.gl.render", "BounceScape2");
myrender.ortho = 0;
myrender.erase_color = [0,0,0,1];
myrender.camera = [0.,-3.5,1.];
myrender.lookat = [0., 0., 0.];
myrender.light_position = [-.6,1.2,1.5,0.];

var theta = 1.0;
var speed = .01; //Player Speed
var xVel = .01;
var yVel = .01;
var decay = .999;
var ballTheta = 1.0;
var ballSpeed = .01;
var ballXVel = new Array(ObjectCount);
var ballYVel = new Array(ObjectCount);
        ballXVel = [.01,.01];
        ballYVel = [.01,.01];
var ballMass = new Array(ObjectCount);

    // rendering block...
    myrender.erase(); // erase the drawing context
    myrender.drawclients(); // draw the client objects
    myrender.swap(); // swap in the new drawing

// create an array of [jit.gl.gridshape] objects randomly arrayed across the window
var poolBalls = new Array(ObjectCount);
var mySphere = new Array(MySphereCount);
createBalls();
createMySpheres();

function cameraView(a,b,c,x,y,z)
{
myrender.camera = [a, b, c];
myrender.lookat = [x, y, z];
}

function createMySpheres()
    {
    for(var i=0;i<MySphereCount;i++)
        {
        mySphere[i] = new JitterObject("jit.gl.gridshape","BounceScape2");
        mySphere[i].shape = "sphere";
        mySphere[i].lighting_enable = 1;
        mySphere[i].smooth_shading = 1;
        mySphere[i].scale = [0.2,0.2,0.2];
        mySphere[i].color = [1,1,1,1.];
        mySphere[i].blend_enable = 1;
        mySphere[i].depth_enable = 1;
        mySphere[i].position = [0,0,0];
        }
    }

function createBalls()
    {
// initialize our little spheres with random colors and positions (x,y,z)
for(var i=0;i<ObjectCount;i++) 
        { 
        poolBalls[i] = new JitterObject("jit.gl.gridshape","BounceScape2");
        poolBalls[i].shape = "sphere";
        poolBalls[i].lighting_enable = 1;
        poolBalls[i].smooth_shading = 1;
        poolBalls[i].scale = [0.1,0.1,0.1];
        //poolBalls[i].color = [Math.random(),Math.random(),Math.random(),0.5] ;
  
        poolBalls[i].position = [Math.random()*2.-1, Math.random()*2.-1, Math.random()*2.-1];
        poolBalls[i].blend_enable = 1;
        }
poolBalls[0].color = [1.,0.,0.,1.] ;
poolBalls[1].color = [0.,1.,0.,1.] ;
    }

function resetPosition()
    {
    mySphere.position = [0,0,0];
    // rendering block...
    myrender.erase(); // erase the drawing context
    myrender.drawclients(); // draw the client objects
    myrender.swap(); // swap in the new drawing
    createBalls();
    } 

function bang() 
// main drawing loop... figure out which little spheres to move 
// and drive the renderer
{
    // collision detection block. we need to iterate through 
    // the little spheres and check their distance from the control 
    // object. if we're touching we move the little sphere away 
    // along the correct angle of contact.
    
            var pendaDistance_x = mySphere[0].position[0] - mySphere[1].position[0];
            var pendaDistance_y = mySphere[0].position[1] - mySphere[1].position[1];
            var pendaDistance_z = mySphere[0].position[2] - mySphere[1].position[2];

            var pendaDistance = Math.sqrt(pendaDistance_x*pendaDistance_x+pendaDistance_y*pendaDistance_y+pendaDistance_z*pendaDistance_z);

            outlet(3, pendaDistance);


            if (pendaDistance <= 0.4)
                {
                outlet(3, pendaDistance);
                outlet(4, bang);
                }
    
    
    for(var i = 0;i<ObjectCount;i++) { 
        for(var j = 0;j<MySphereCount;j++)
            {
        // cartesian distance along the x and y axis
        var distx = mySphere[j].position[0]-poolBalls[i].position[0]; 
        var disty = mySphere[j].position[1]-poolBalls[i].position[1]; 
        var distz = mySphere[j].position[2]-poolBalls[i].position[2]; 

        /*for(var k = 1; k<MySphereCount; k++)
            {
            var pendaDistance_x = mySphere[j].position[0] - mySphere[k].position[0];
            var pendaDistance_y = mySphere[j].position[1] - mySphere[k].position[1];
            var pendaDistance_z = mySphere[j].position[2] - mySphere[k].position[2];

            var pendaDistance = Math.sqrt(pendaDistance_x*pendaDistance_x+pendaDistance_y*pendaDistance_y+pendaDistance_z*pendaDistance_z);

            outlet(3, pendaDistance);


            if (pendaDistance <= 0.3)
                {
                outlet(3, pendaDistance);
                outlet(4, bang);
                }
            }
            */

        // polar distance between the two objects
        var r = Math.sqrt(distx*distx+disty*disty+distz*distz); 
        // angle of little sphere around control object
        ballTheta = Math.atan2(disty,distx); 
        ballMass[i] = 1.0;

        // check for collision...
        if(r<0.3) 
        // control object is size 0.1, little spheres are 0.05, 
        // so less than 0.15 and it's a hit...
            {
            outlet(0,bang);
            // convert polar->cartesian to figure out x and y displacement
            //var movex = (0.15-r)*Math.cos(ballTheta); 
            //var movey = (0.15-r)*Math.sin(ballTheta); 
            //ballSpeed = speed;
            ballXVel[i] = Math.cos(ballTheta) * Math.abs(xVel * 1.5);
            ballYVel[i] = Math.sin(ballTheta) * Math.abs(yVel *1.5);

            outlet(1,ballSpeed, ballXVel, ballYVel, xVel, yVel);

            poolBalls[i].position = [poolBalls[i].position[0] - ballXVel[i], poolBalls[i].position[1] - ballYVel[i]];


            // offset the little sphere to the new position, 
            // which should be just beyond touching at the 
            // angle of contact we had before. the result 
            // should look like we've "pushed" it along...
            }
        else 
            {
            if (poolBalls[i].position[0] >= 1.0)
                {
                poolBalls[i].position = [.99 ,poolBalls[i].position[1], poolBalls[i].position[2]];
                ballXVel[i] = -ballXVel[i];
                outlet(5, bang);
                }
            else if (poolBalls[i].position[0] <= -1.0)
                {
                poolBalls[i].position = [-.99 ,poolBalls[i].position[1], poolBalls[i].position[2]];
                ballXVel[i] = -ballXVel[i];
                outlet(5, bang);
                }

            if (poolBalls[i].position[1] >= 1.0)
                {
                poolBalls[i].position = [poolBalls[i].position[0], .99, poolBalls[i].position[2]];
                ballYVel[i] = -ballYVel[i];
                outlet(5, bang);
                }
            else if (poolBalls[i].position[1] <= -1.0)
                {
                poolBalls[i].position = [poolBalls[i].position[0], -.99, poolBalls[i].position[2]];
                ballYVel[i] = -ballYVel[i];
                outlet(5, bang);
                }
            if (poolBalls[i].position[2] >= 1.0)
                {
                poolBalls[i].position = [poolBalls[i].position[0], poolBalls[i].position[1], .99];
                ballZVel[i] = -ballZVel[i];
                outlet(5, bang);
                }
            else if (poolBalls[i].position[2] <= -1.0)
                {
                poolBalls[i].position = [poolBalls[i].position[0], poolBalls[i].position[1], -.99];
                ballZVel[i] = -ballZVel[i];
                outlet(5, bang);
                }

            poolBalls[i].position = [poolBalls[i].position[0] - ballXVel[i], poolBalls[i].position[1] - ballYVel[i]];
            outlet(6, poolBalls[i].position[0], poolBalls[i].position[1]);
            }
        }
    // rendering block...
    myrender.erase(); // erase the drawing context
    myrender.drawclients(); // draw the client objects
    myrender.swap(); // swap in the new drawing
}
}

function manualPosition(x,y,z)
    {
    mySphere[0].position = [x,y,z];
    myrender.erase(); // erase the drawing context
    myrender.drawclients(); // draw the client objects
    myrender.swap(); // swap in the new drawing
    }

function fullscreen(v) 
// function to send the [jit.window] into fullscreen mode
    {
    mywindow.fullscreen = v;
    }

function manualPosition2(x,y,z)
    {
    mySphere[1].position = [x,y,z];
    myrender.erase(); // erase the drawing context
    myrender.drawclients(); // draw the client objects
    myrender.swap(); // swap in the new drawing
    }

function lightPosition(w,x,y,z)
    {
    myrender.light_position = [w,x,y,z];
    }
