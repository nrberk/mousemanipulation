/*
 * 
 */

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

@SuppressWarnings("serial")
public class GraphPanel extends JPanel {
	private PhysicsPanel heatPanel;
	private JPanel controlPanel, rightPanel;
	private JSplitPane joyPanel;
	
	private JLabel errorMsg;
	private JTextField connectionPort;
	private JButton connectButton;
	private JLabel t1Info, t2Info, kConstantInfo;
	private JComboBox constantSelector;
	private double kConstant;
	private JSlider T1, T2;
	
	private Timer timer;
	private static final int DELAY = 0;
	private DatagramSocket dsocket;
	private DatagramPacket packet;
	private byte[] buffer;
	private int port;
	private final int T_MIN = 0, T_MAX = 100, T_INIT = 50;	// change here for temperature range
	private int tempOne, tempTwo;
	
	public GraphPanel(int width, int height) {
		// left
		heatPanel = new PhysicsPanel();
		//heatPanel.setPreferredSize(new Dimension(400,600));
		heatPanel.setSize(new Dimension(500,800));
		
		// right
		controlPanel = new JPanel(new GridLayout(4, 2));
		
		connectionPort = new JTextField("21567");
		connectButton = new JButton("Connect!");
		connectButton.addActionListener(new ButtonListener());
		
		kConstantInfo = new JLabel("Select object's thermal conductivity constant");
		String[] kConstant_Value = {"1.1", "2.2", "3.3", "4.4", "5.5"};
		constantSelector = new JComboBox(kConstant_Value);
		constantSelector.addActionListener(new ComboBoxListener());
		
		t1Info = new JLabel("Enter temperature for one end:");
		t2Info = new JLabel("Enter temperature for the other end:");
		T1 = new JSlider(JSlider.HORIZONTAL, T_MIN, T_MAX, T_INIT);
		T1.addChangeListener(new SliderListener());
		T2 = new JSlider(JSlider.HORIZONTAL, T_MIN, T_MAX, T_INIT);
		T2.addChangeListener(new SliderListener());
		
		T1.setMajorTickSpacing(10);
		T1.setMinorTickSpacing(1);
		T1.setPaintTicks(true);
		T1.setPaintLabels(true);
		
		T2.setMajorTickSpacing(10);
		T2.setMinorTickSpacing(1);
		T2.setPaintTicks(true);
		T2.setPaintLabels(true);
		
		controlPanel.add(connectionPort);
		controlPanel.add(connectButton);
		controlPanel.add(kConstantInfo);
		controlPanel.add(constantSelector);
		controlPanel.add(t1Info);
		controlPanel.add(T1);
		controlPanel.add(t2Info);
		controlPanel.add(T2);
		controlPanel.setPreferredSize(new Dimension(800,800));
		
		errorMsg = new JLabel("haha");
		
		rightPanel = new JPanel(new GridLayout(2, 1));
		rightPanel.add(errorMsg);
		rightPanel.add(controlPanel);
		//rightPanel.setPreferredSize(new Dimension(20,20));
		
		// joyPanel is the entire panel
		joyPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, heatPanel, controlPanel);
		joyPanel.setSize(new Dimension(1000, 800));
		//joyPanel.setOneTouchExpandable(true);
		joyPanel.setDividerLocation(500);

		heatPanel.setMinimumSize(new Dimension(500, 800));
		controlPanel.setMinimumSize(new Dimension(500, 800));
		
		add(joyPanel);
		setSize(new Dimension(width,height));
		
	}	// end of ControlPanel
	
	private boolean initConnection() {
		try {			
			dsocket = new DatagramSocket(port);
			buffer = new byte[1024];
			packet = new DatagramPacket(buffer, buffer.length);
			
			timer = new Timer(DELAY, new MovingBallListener());
			timer.start();
		} catch(Exception e) {
			System.out.println("haha");
			System.err.println(e);
			return false;
		}	// end of try-catch
		
		return true;
	}
	
	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			Object action = event.getSource();
			
			if(action.equals(connectButton)) {
				boolean connected;
				
				do {
					port = Integer.parseInt(connectionPort.getText());
					connected = initConnection();
					
					if(!connected) {
						errorMsg.setText("Unable to connect, please check port.");
					}
				} while(!connected);
			}
		}
	}	// end of ButtonListener
	
	private class ComboBoxListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JComboBox comboBox = (JComboBox)e.getSource();
	        kConstant = Double.parseDouble((String)comboBox.getSelectedItem());
	        heatPanel.setKConstant(kConstant);
	    }
	}
	
	private class MovingBallListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				double[] pendaCoor = new double[6];
				dsocket.receive(packet);
				String msg = new String(buffer, 0, packet.getLength());
				String[] tokens = msg.split(",");
				
				for(int i = 0; i < 6; i++)
					pendaCoor[i] = Double.parseDouble(tokens[i]);

				packet.setLength(buffer.length);
				
				heatPanel.setAxis(pendaCoor[0], pendaCoor[1], pendaCoor[2], pendaCoor[3], pendaCoor[4], pendaCoor[5]);
			} catch(Exception ex) {
				System.err.println(ex);
			}
		}
	}	// end of MovingBallListener
	
	private class SliderListener implements ChangeListener {
	    public void stateChanged(ChangeEvent e) {
	        JSlider source = (JSlider)e.getSource();
	        
	        if(source.equals(T1)) {
	        	tempOne = (int)source.getValue();
	        }
	        else if(source.equals(T2)) {
	        	tempTwo = (int)source.getValue();
	        }
	        
	        heatPanel.setTemp(tempOne, tempTwo);
	    }
	}	// end of SliderListener
}	// end of class ControlPanel
