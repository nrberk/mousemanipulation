/*
 * Z-axis of the ball is represented by the size of the object.
 */

import java.awt.*;
import javax.swing.*;
import java.lang.Math;

@SuppressWarnings("serial")
public class PhysicsPanel extends JPanel {
	private static final int CIRCLE_DIAMETER = 4;
	private int x1, y1, z1, x2, y2, z2;
	private double distance, kConstant, t1, t2, qOvert, area;
	
	public PhysicsPanel() {
		x1 = 0;
		y1 = 0;
		z1 = 0;
		
		x2 = 0;
		y2 = 0;
		z2 = 0;
		
		distance = 0;
		kConstant = 0;
		t1 = 0;
		t2 = 0;
		qOvert = 0;
		
		setBackground(Color.BLACK);
	}
	
	public PhysicsPanel(int ballOneX, int ballOneY, int ballOneZ, int ballTwoX, int ballTwoY, int ballTwoZ) {
		setAxis(ballOneX, ballOneY, ballOneZ, ballTwoX, ballTwoY, ballTwoZ);

		kConstant = 0;
		area = 0;
		t1 = 0;
		t2 = 0;
		qOvert = 0;
		
		setBackground(Color.BLACK);
	}
	
	public PhysicsPanel(int ballOneX, int ballOneY, int ballOneZ, int ballTwoX, int ballTwoY, int ballTwoZ, double newKConstant, double newArea, double tempOne, double tempTwo) {
		setAxis(ballOneX, ballOneY, ballOneZ, ballTwoX, ballTwoY, ballTwoZ);
		
		setKConstant(newKConstant);
		setArea(newArea);
		setTemp(tempOne, tempTwo);
		
		setBackground(Color.BLACK);
	}
	
	public void paintComponent(Graphics page) {
		super.paintComponent(page);
		
		page.setColor(Color.RED);
		page.fillOval(x1, y1, CIRCLE_DIAMETER, CIRCLE_DIAMETER);
		
		page.setColor(Color.BLUE);
		page.fillOval(x2, y2, CIRCLE_DIAMETER, CIRCLE_DIAMETER);
	}
	 
	public void setAxis(double ballOneX, double ballOneY, double ballOneZ, double ballTwoX, double ballTwoY, double ballTwoZ) {
		x1 = (int)(ballOneZ * 100 * Math.cos(ballOneY) * Math.sin(ballOneX)) + 100;
		y1 = (int)(ballOneZ * 100 * Math.cos(ballOneX) * Math.sin(ballOneY)) + 100;
		z1 = (int)(ballOneZ * 100 * Math.abs(Math.cos(ballOneX)) * Math.abs(Math.cos(ballOneY)));
		
		x2 = (int)(ballTwoZ * 100 * Math.cos(ballTwoY) * Math.sin(ballTwoX)) + 100;
		y2 = (int)(ballTwoZ * 100 * Math.cos(ballTwoX) * Math.sin(ballTwoY)) + 100;
		z2 = (int)(ballTwoZ * 100 * Math.abs(Math.cos(ballTwoX)) * Math.abs(Math.cos(ballTwoY)));
		
		distance = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2) + Math.pow((z2 - z1), 2));
			
		updateUI();
	 }
	
	public void setTemp(double tempOne, double tempTwo) {
		t1 = tempOne;
		t2 = tempTwo;
		
		// Q/t = k*A*(T1-T2)/L
		qOvert = kConstant * area * (t1 - t2) / distance;
	}
	
	public void setKConstant(double newK) {
		kConstant = newK;
	}
	
	public void setArea(double newArea) {
		area = newArea;
	}
	
	public double getqOvert() {
		return qOvert;
	}
}